<?php /* Template Name: Custom Fields */ ?>

<?php get_header(); ?>
    <div class="template-description">
        <h2>Custom Fields Template</h2>
        <h5>Example of custom fields in a template.</h5>
    </div>

	<div class="row">
		<div class="col-sm-12">
          <h3>Song Titles</h3>
          <?php $songs = get_post_meta($post->ID, 'song-title', false); ?>
          <?php $background_color = get_post_meta($post->ID, 'background-color', true); ?>
          <ul style="background-color:<?php echo $background_color; ?>">
              <?php foreach($songs as $song) {
                  echo '<li>'.$song.'</li>';
                  } ?>
          </ul>

		</div> <!-- /.col -->
    <!-- No sidebar, full width -->
	</div> <!-- /.row -->
	<div class="row">
		<div class="col-sm-8">

			<?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post();
  	
					get_template_part( 'content', get_post_format() );
  
				endwhile; endif; 
			?>

		</div> <!-- /.col -->
    
    <?php get_sidebar(); ?>
	</div> <!-- /.row -->
<?php get_footer(); ?>