<?php get_header(); ?>
  <div class="template-description">
    <h2>Posts Grid Template</h2>
    <h5>Grid that displays all posts.</h5>
  </div>
<?php
$counter = 1; //start counter

$grids = 4; //Grids per row

if ( have_posts() ) : while ( have_posts() ) : the_post();
  if($counter == 1) {
    ?>
      <div class="template-grid-row row">
    <?php
  }
?>
		<div class="template-grid-col col-xs-6 col-sm-<?php echo 12 / $grids; ?>">
          <?php get_template_part( 'grid-content', get_post_format() ); ?>
		</div> <!-- /.col -->
<?php
  if($counter == $grids) {
    $counter = 1;
    ?>
      </div>
    <?php
  } else {
    $counter++;
  }

endwhile;
//Pagination can go here if you want it.
endif;
        
?>
<?php get_footer(); ?>