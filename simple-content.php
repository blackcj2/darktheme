<div class="blog-post">
	<h2 class="blog-post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
    <!-- Author and post date removed for simple content -->
    <?php the_content(); ?>
</div><!-- /.blog-post -->