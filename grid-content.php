<div class="grid-content">
  <div class="postimage">
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'thumbnail' ); ?></a>
  </div>
  <h4><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
  <p><?php echo the_excerpt_max_charlength( 140 ) ?></p>
</div>
