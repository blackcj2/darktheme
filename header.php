<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dark_Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href="<?php bloginfo('template_directory');?>/style.css" rel="stylesheet">
    <?php wp_head(); ?>
        <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script>
      $(document).ready(function() {
        if ($('#countdown').length){
            setInterval(function(){ 
              var future = new Date($('#countdown').attr('datetime'));
              var today = new Date();
              console.log(today - future);
            }, 1000);
        }
      });
    </script>
</head>
<body>
  <div id="wrapper">
    <div class="blog-masthead">
      <div class="container">
        <div class="blog-header">
          <img src="<?php bloginfo('template_directory');?>/cooking-pot-xxl.png" width="100px" height="100px" style="float: left; margin-right:20px;">
          <h1 class="blog-title"><a href="<?php bloginfo('wpurl');?>"><?php echo get_bloginfo( 'name' ); ?></a></h1>
          <p class="lead blog-description"><?php echo get_bloginfo( 'description' ); ?></p>
        </div>
        <nav class="blog-nav">
          <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); /*wp_list_pages( '&title_li=');*/ ?>
        </nav>
      </div>
    </div>
    <div class="container" id="main-content">

