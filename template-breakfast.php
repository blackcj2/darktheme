<?php /* Template Name: Breakfast Template */ ?>

<?php get_header(); ?>
  <div class="template-description">
    <h2>Breakfast Template</h2>
    <h5>Page that displays only breakfast posts.</h5>
  </div>
	<div class="row">
		<div class="col-sm-8">

			<?php 
				query_posts('category_name=breakfast'); while ( have_posts() ) : the_post();
  	
					get_template_part( 'simple-content', get_post_format() );
  
				endwhile; 
                wp_reset_query();
			?>

		</div> <!-- /.col -->
    
        <?php get_sidebar(); ?>
      
	</div> <!-- /.row -->

<?php get_footer(); ?>