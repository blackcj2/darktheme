<?php /* Template Name: HTML Template */ ?>
<!-- This template displays HTML and does not use the content provided by Wordpress -->
<?php get_header(); ?>
    <div class="template-description">
        <h2>Custom HTML Template</h2>
      <h5>Example of custom HTML in a template.</h5> 
      <a href="https://bitbucket.org/blackcj2/darktheme/src">Source Code for this theme</a>
    </div>
	<div class="row">
        <div class="col-sm-2" style="height: 200px; padding: 10px;">

		</div> <!-- /.col -->
		<div class="col-sm-8" style="background: white; height: 200px;">
          <p style="font-size:2em; padding:50px 10px 10px 10px; text-align:center;">Welcome to Recipe Box. On this website you'll find recipes for a variety of different meals along with tips and tricks for cooking food.</p>
		</div> <!-- /.col -->
        <div class="col-sm-2" style="height: 200px; padding: 10px;">

		</div> <!-- /.col -->
	</div> <!-- /.row -->
	<div class="row">
		<div class="col-sm-12" style="background: white; height: 300px;">
        <?php
        $counter = 1; //start counter

        $grids = 3; //Grids per row

        query_posts('category_name=breakfast'); while ( have_posts() && $counter <= 3 ) : the_post();
          if($counter == 1) {
            ?>
              <div class="template-grid-row row">
            <?php
          }
        ?>
                <div class="template-grid-col col-xs-12 col-sm-<?php echo 12 / $grids; ?>">
                  <?php get_template_part( 'grid-content', get_post_format() ); ?>
                </div> <!-- /.col -->
        <?php
          if($counter == 3) {
            ?>
              </div>
            <?php
          }else {
            $counter++;
          }

        endwhile;
        wp_reset_query();
        ?>
		</div> <!-- /.col -->
	</div> <!-- /.row -->
<?php get_footer(); ?>